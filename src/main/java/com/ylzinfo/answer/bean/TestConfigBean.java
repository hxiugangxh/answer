package com.ylzinfo.answer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cm_test_config")
public class TestConfigBean {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "int null comment '试卷id'")
    private Long cardId;
    @Column(columnDefinition = "varchar(100) null comment '题目类型 SINGLE单选 MULTI多选 FILL_BLANK填空 JUDGE判断 ANSWER简答'")
    private String answerType;
    @Column(columnDefinition = "int null comment '题目数量'")
    private Integer questionAmount;
}
