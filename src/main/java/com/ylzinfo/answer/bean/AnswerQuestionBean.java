package com.ylzinfo.answer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 答题卡对应的题目
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CM_ANSWER_QUESTION")
public class AnswerQuestionBean implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long answerCardId;

    @Column(columnDefinition = "int(11) DEFAULT NULL COMMENT '索引(1,2,3)题号'")
    private Integer num;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '问题'")
    private String question;

    @Column(columnDefinition = "varchar(2000) DEFAULT NULL COMMENT '答案（判断题：对；错）'")
    private String answer;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '(SINGLE：单选；MULTI：多选；JUDGE：判断；FILL_BLANK：填空；ANSWER：问答)'")
    private String answerType;

    @Column(columnDefinition = "varchar(2000) DEFAULT NULL COMMENT '解析信息'")
    private String parseInfo;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "answerQuestionId")
    private List<AnswerOptionsBean> answerOptionsBeanList = new ArrayList<>();

}
