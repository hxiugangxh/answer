package com.ylzinfo.answer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 答题卡对应选项
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CM_ANSWER_OPTIONS")
public class AnswerOptionsBean implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long answerQuestionId;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT ' 选项(A B C D....)'")
    private String selectOption;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '选项信息（填空题则为答案）'")
    private String optionInfo;

}
