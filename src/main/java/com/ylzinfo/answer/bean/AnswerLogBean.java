package com.ylzinfo.answer.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "CM_ANSWER_LOG")
public class AnswerLogBean implements Serializable {

    public AnswerLogBean() {
    }

    public AnswerLogBean(Long id, String userName, Long answerCardId, Integer totalCorrect, Date createTime) {
        this.id = id;
        this.userName = userName;
        this.answerCardId = answerCardId;
        this.totalCorrect = totalCorrect;
        this.createTime = createTime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '用户ID'")
    private String userName;
    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '答题卡'")
    private Long answerCardId;
    @Column(columnDefinition = "timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date createTime;
    @Column(columnDefinition = "int null comment '选择题总数'")
    private Integer singleCount;
    @Column(columnDefinition = "int null comment '选择题正确数'")
    private Integer singleCorrect;
    @Column(columnDefinition = "int null comment '多选题总数'")
    private Integer multiCount;
    @Column(columnDefinition = "int null comment '多选题正确数'")
    private Integer multiCorrect;
    @Column(columnDefinition = "int null comment '填空题总数'")
    private Integer fillBlankCount;
    @Column(columnDefinition = "int null comment '填空题正确数'")
    private Integer fillBlankCorrect;
    @Column(columnDefinition = "int null comment '判断题总数'")
    private Integer judgeCount;
    @Column(columnDefinition = "int null comment '判断题正确数'")
    private Integer judgeCorrect;
    @Column(columnDefinition = "int null comment '题目总数（除简答题）'")
    private Integer totalCount;
    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '正确题目总数（除简答题）'")
    private Integer totalCorrect;
    @Column(columnDefinition = "varchar(100) null comment '答题类型 STUDY学习 TEST考试 ERROR错题'")
    private String logType;

    @Transient
    private String answerCardTitle;
    @Transient
    private String answerType;
    @Transient
    private String answerTypeName;
    @Transient
    private List<AnswerErrorBean> errorList = new ArrayList<>();
}
