package com.ylzinfo.answer.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 答题卡
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CM_ANSWER_CARD")
public class AnswerCardBean implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '标题'")
    private String title;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '隶属类型'")
    private String answerType;

    @Column(columnDefinition = "int(11) DEFAULT NULL COMMENT '答题时间（默认60分；单位：分）'")
    private Integer answerTime;

    @Column(columnDefinition = "timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+08:00")
    private Date createTime;

    @Transient
    private String answerTypeName;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "answerCardId")
    @OrderBy("answerType asc, num asc")
    private List<AnswerQuestionBean> answerQuestionBeanList = new ArrayList<>();
}
