package com.ylzinfo.answer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CM_USER")
public class UserBean implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '账号'")
    private String username;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '密码'")
    private String password;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '名称'")
    private String displayName;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '头像imageUrl'")
    private String avatar = "/img/avatar.jpg";


}
