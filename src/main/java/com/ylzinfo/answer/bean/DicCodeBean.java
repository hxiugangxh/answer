package com.ylzinfo.answer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CM_DIC_CODE")
public class DicCodeBean implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '编码（属性名大写）'")
    private String code;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '键'")
    private String dicKey;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '值'")
    private String dicValue;

    @Transient
    private Integer answerTypeCount;
    @Transient
    private List<DicCodeBean> list;
}
