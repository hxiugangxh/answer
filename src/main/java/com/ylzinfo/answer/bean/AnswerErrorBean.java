package com.ylzinfo.answer.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CM_ANSWER_ERROR")
public class AnswerErrorBean implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "int(11) DEFAULT NULL COMMENT '错题卡ID'")
    private Long answerLogId;

    @Column(columnDefinition = "int(11) DEFAULT NULL COMMENT '答题卡ID'")
    private Long answerCardId;

    @Column(columnDefinition = "varchar(255) DEFAULT NULL COMMENT '题目ID'")
    private String answerQuestionId;

    @Column(columnDefinition = "varchar(100) null comment '题型'")
    private String questionType;
}
