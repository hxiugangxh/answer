package com.ylzinfo.answer;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Filter;
import com.ylzinfo.answer.bean.*;
import com.ylzinfo.answer.dao.jpa.*;
import com.ylzinfo.answer.parser.service.ParserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class InitAnswerDataRunner implements ApplicationRunner {
    @Autowired
    private AnswerCardBeanJpa answerCardBeanJpa;

    @Autowired
    private AnswerQuestionBeanJpa answerQuestionBeanJpa;

    @Autowired
    private AnswerOptionsBeanJpa answerOptionsBeanJpa;

    @Autowired
    private DicCodeBeanJpa dicCodeBeanJpa;

    @Autowired
    private UserBeanJpa userBeanJpa;

    @Autowired
    private ParserService parserService;

    @Transactional
    public void initData() throws IOException {
        initDicCodeBean();
//        initAnswerCardBean();
//        initAnswerQuestionBean();
//        initAnswerOptionsBean();
        initUserBean();
        initFile();
    }

    private void initUserBean() {
        Iterable<UserBean> all = userBeanJpa.findAll();
        if (!all.iterator().hasNext()) {
            List<UserBean> userBeanList = Arrays.asList(
                    new UserBean(null, "admin", "123456", "管理员", "/img/avatar.jpg"),
                    new UserBean(null, "zhangsan", "123456", "张三", "/img/avatar.jpg"),
                    new UserBean(null, "lisi", "123456", "李四", "/img/avatar.jpg")
            );

            userBeanJpa.save(userBeanList);
        }
    }

    private void initDicCodeBean() {
        Iterable<DicCodeBean> all = dicCodeBeanJpa.findAll();
        if (!all.iterator().hasNext()) {
            List<DicCodeBean> dicBelongTypeBeanList = Arrays.asList(
                    new DicCodeBean(null, "ANSWER_TYPE", "CODE", "新题库", null, null),
                    new DicCodeBean(null, "ANSWER_TYPE", "ERROR", "错题集", null, null)
            );

            dicCodeBeanJpa.save(dicBelongTypeBeanList);
        }
    }

    private void initAnswerCardBean() {
        Iterable<AnswerCardBean> all = answerCardBeanJpa.findAll();
        if (!all.iterator().hasNext()) {
            List<AnswerCardBean> answerCardBeanList = Arrays.asList(
                    new AnswerCardBean(null, 1l, "Java基础", "CODE", 1000, new Date(), "", new ArrayList<>()),
                    new AnswerCardBean(null, 1l, "C++基础", "CODE", 1000, new Date(), "", new ArrayList<>()),
                    new AnswerCardBean(null, 1l, "PHP基础", "CODE", 1000, new Date(), "", new ArrayList<>())
            );

            answerCardBeanJpa.save(answerCardBeanList);
        }
    }

    private void initAnswerQuestionBean() {
        Iterable<AnswerQuestionBean> all = answerQuestionBeanJpa.findAll();
        if (!all.iterator().hasNext()) {
            List<AnswerQuestionBean> answerQuestionBeanList = Arrays.asList(
                    new AnswerQuestionBean(null, 1l, 1, "下列哪一个 Transact-SQL 语句能够实现收回 user2 查询基本表 T 的权限？（ ）", "A", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 2, "数据库管理系统的工作不包括？（）", "C", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 3, "在关系数据库设计中，设计关系模式（二维表）是数据库设计中哪个阶段的任务。（）", "B", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 4, "（） 是存储在计算机内有结构的数据的集合。", "C", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 5, "层次型、网状型和关系型数据库划分原则是。 （）", "D", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 6, "实体是信息世界中的术语，与之对应的数据库术语为。 （）", "C", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 7, "表 test1 中包含两列： c1 为整型， c2 为 8 位长的字符串类型，使用如下语句创建视图：\n" +
                            "\n" +
                            "CREATE VIEW v1 AS SELECT c1,c2 FROM test1 WHERE c1>30 WITH CHECK OPTION\n" +
                            "\n" +
                            "以下语句能够成功执行的有几条？ （ ）\n" +
                            "\n" +
                            "INSERT INTO v1 VALUES(1, ' 赵六 ')\n" +
                            "\n" +
                            "INSERT INTO v1 VALUES(101, ' 李四 ')\n" +
                            "\n" +
                            "INSERT INTO t1 VALUES(20, ' 王五 ')", "A", "SINGLE", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 1, "用如下的 SQL 语句创建一个 s 表\n" +
                            "\n" +
                            "CREATE  TABLE  s （ NO  CHAR （ 4 ） primary key ，\n" +
                            "\n" +
                            "NAME  CHAR （ 8 ） NOT  NULL ，\n" +
                            "\n" +
                            "SEX  CHAR （ 2 ），\n" +
                            "\n" +
                            "AGE  INT ）\n" +
                            "\n" +
                            "那么可以插入到该表中的数据是（ ）", "B★C", "MULTI", "解析信息", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 1, "1 + 1 = 2?", "对", "JUDGE", "根据数学加法是正确的", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 1, "1 + 1 = __A__， 2 + 2 = __B__", "2★4", "FILL_BLANK", "根据数学加法是正确的", new ArrayList<>()),
                    new AnswerQuestionBean(null, 1l, 1, "简述关系模型的三类完整性约束。", "不知道不会做", "ANSWER", "解析信息", new ArrayList<>())
            );

            answerQuestionBeanJpa.save(answerQuestionBeanList);
        }
    }

    private void initAnswerOptionsBean() {
        Iterable<AnswerOptionsBean> all = answerOptionsBeanJpa.findAll();
        if (!all.iterator().hasNext()) {
            List<AnswerOptionsBean> answerOptionsBeanList = Arrays.asList(
                    new AnswerOptionsBean(null, 1l, "A", "REVOKE UPDATE ON T"),
                    new AnswerOptionsBean(null, 1l, "B", "GRANT SELECT ON T TO user2"),
                    new AnswerOptionsBean(null, 1l, "C", "DENY SELECT ON T TO user2"),
                    new AnswerOptionsBean(null, 1l, "D", "REVOKE SELECT ON T FROM user2"),
                    new AnswerOptionsBean(null, 2l, "A", "（’1031’，’曾华’，NULL，NULL）"),
                    new AnswerOptionsBean(null, 2l, "B", "（’1032’，’曾华’，男，23）"),
                    new AnswerOptionsBean(null, 2l, "C", "（NULL，’曾华’，’男’，’23’）"),
                    new AnswerOptionsBean(null, 2l, "D", "（’1033’，NULL，’男’，23）"),
                    new AnswerOptionsBean(null, 3l, "A", "定义数据库"),
                    new AnswerOptionsBean(null, 3l, "B", "对已定义的数据库进行管理"),
                    new AnswerOptionsBean(null, 3l, "C", "为定义的数据库提供操作系统"),
                    new AnswerOptionsBean(null, 3l, "D", "数据通信"),
                    new AnswerOptionsBean(null, 4l, "A", "逻辑设计阶段"),
                    new AnswerOptionsBean(null, 4l, "B", "概念设计阶段"),
                    new AnswerOptionsBean(null, 4l, "C", "物理设计阶段"),
                    new AnswerOptionsBean(null, 4l, "D", "需求分析阶段"),
                    new AnswerOptionsBean(null, 5l, "A", "数据库系统"),
                    new AnswerOptionsBean(null, 5l, "B", "数据库"),
                    new AnswerOptionsBean(null, 5l, "C", "数据库管理系统"),
                    new AnswerOptionsBean(null, 5l, "D", "数据结构"),
                    new AnswerOptionsBean(null, 6l, "A", "记录长度"),
                    new AnswerOptionsBean(null, 6l, "B", "文件的大小"),
                    new AnswerOptionsBean(null, 6l, "C", "联系的复杂程度"),
                    new AnswerOptionsBean(null, 6l, "D", "数据之间的联系"),
                    new AnswerOptionsBean(null, 7l, "A", "文件"),
                    new AnswerOptionsBean(null, 7l, "B", "数据库"),
                    new AnswerOptionsBean(null, 7l, "C", "字段"),
                    new AnswerOptionsBean(null, 7l, "D", "记录"),
                    new AnswerOptionsBean(null, 8l, "A", "0"),
                    new AnswerOptionsBean(null, 8l, "B", "1"),
                    new AnswerOptionsBean(null, 8l, "C", "2"),
                    new AnswerOptionsBean(null, 8l, "D", "3"),
                    new AnswerOptionsBean(null, 9l, "对", ""),
                    new AnswerOptionsBean(null, 9l, "错", ""),
                    new AnswerOptionsBean(null, 10l, "A", "2"),
                    new AnswerOptionsBean(null, 10l, "B", "4")
            );

            answerOptionsBeanJpa.save(answerOptionsBeanList);
        }
    }

    private void initFile() throws IOException {
        List<InputStream> inputStreamList = Arrays.asList(
                new ClassPathResource("file/test1.docx").getInputStream(),
//                new ClassPathResource("file/test2.docx").getInputStream(),
//                new ClassPathResource("file/test3.docx").getInputStream(),
                new ClassPathResource("file/新题库.docx").getInputStream()
        );
        Iterable<AnswerCardBean> all = answerCardBeanJpa.findAll();
        AnswerCardBean one = CollUtil.findOne(all, new Filter<AnswerCardBean>() {
            @Override
            public boolean accept(AnswerCardBean answerCardBean) {
                return "扶贫攻坚".equals(answerCardBean.getTitle())
                        || "宪法及相关法律知识".equals(answerCardBean.getTitle())
                        || "就业创业".equals(answerCardBean.getTitle())
                        || "人才队伍建设与人事制度改革".equals(answerCardBean.getTitle())
                        || "劳动关系与工资收入分配".equals(answerCardBean.getTitle())
                        || "宪法及相关法律知识".equals(answerCardBean.getTitle())
                        || "社会保障".equals(answerCardBean.getTitle());
            }
        });
        if (one == null) {
            for (InputStream inputStream : inputStreamList) {
                parserService.insertByResourceFile(inputStream);
            }
        }
    }

    @Override
    public void run(ApplicationArguments var1) throws Exception {
        initData();
    }

}
