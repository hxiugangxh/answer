package com.ylzinfo.answer.vo;

import lombok.Data;

import java.util.Date;

@Data
public class SignInVo {
    private String userName;
    private Date beginOfDay;
    private Date endOfDay;
}
