package com.ylzinfo.answer.vo;

import lombok.Data;

@Data
public class PaperAnalyseVo {
    private String questionType;
    private String correctCount;
    private String totalCount;
}
