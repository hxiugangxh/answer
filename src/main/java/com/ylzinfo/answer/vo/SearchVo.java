package com.ylzinfo.answer.vo;

import lombok.Data;

@Data
public class SearchVo {
    private String title;
    private String answerType;
}
