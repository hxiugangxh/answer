package com.ylzinfo.answer.parser.service;

import com.ylzinfo.answer.bean.AnswerCardBean;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface ParserService {
    List<AnswerCardBean> preview(File file) throws IOException;

    List<AnswerCardBean> insert(File file) throws IOException;

    List<AnswerCardBean> previewByResourceFile(InputStream inputStream) throws IOException;

    boolean insertByResourceFile(InputStream inputStream) throws IOException;
}
