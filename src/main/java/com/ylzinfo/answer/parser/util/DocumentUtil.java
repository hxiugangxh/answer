package com.ylzinfo.answer.parser.util;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class DocumentUtil {
    public static String getText(File file) throws IOException {
        String name = file.getName();
        if (StrUtil.endWith(name, "docx")) {
            XWPFDocument document = new XWPFDocument(IoUtil.toStream(file));
            XWPFWordExtractor extractor = new XWPFWordExtractor(document);
            return extractor.getText();
        } else if (StrUtil.endWith(name, "doc")) {
            WordExtractor extractor = new WordExtractor(IoUtil.toStream(file));
            return extractor.getText();
        }
        return null;
    }

    public static String getTextByResourceFile(InputStream inputStream) throws IOException {
        XWPFDocument document = new XWPFDocument(inputStream);
        XWPFWordExtractor extractor = new XWPFWordExtractor(document);
        return extractor.getText();
    }
}
