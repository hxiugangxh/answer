package com.ylzinfo.answer.dto;

import lombok.Data;

@Data
public class FormDTO<T> {
    private String msg;
    private int status = 200;
    private T data;
    private String url;

    public FormDTO<T> success(String msg) {
        FormDTO<T> formDTO = new FormDTO<>();
        formDTO.msg = msg;
        return formDTO;
    }

    public FormDTO<T> fail(String msg) {
        FormDTO<T> formDTO = new FormDTO<>();
        formDTO.status = 500;
        formDTO.msg = msg;
        return formDTO;
    }
}
