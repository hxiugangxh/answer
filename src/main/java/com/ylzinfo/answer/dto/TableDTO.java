package com.ylzinfo.answer.dto;

import lombok.Data;

@Data
public class TableDTO<T> {
    private int code;
    private T data;
    private int count;
    private String msg;

    public TableDTO<T> dataSuccess(T data, int count) {
        TableDTO<T> tableDTO = new TableDTO<>();
        tableDTO.data = data;
        tableDTO.count = count;
        tableDTO.code = 0;
        return tableDTO;
    }
}
