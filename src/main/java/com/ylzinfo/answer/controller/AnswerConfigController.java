package com.ylzinfo.answer.controller;

import com.ylzinfo.answer.bean.TestConfigBean;
import com.ylzinfo.answer.service.TestConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/answerConfig")
public class AnswerConfigController {

    @Autowired
    private TestConfigService testConfigService;

    @RequestMapping("/list")
    public String list() {
        return "config/config";
    }

    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public String config(Long id, Model model) {
        List<TestConfigBean> testConfigBeans = testConfigService.getConfigList(id);
        model.addAttribute("config",testConfigBeans);
        model.addAttribute("id",id);
        return "answer/answer_config";
    }

    @RequestMapping("/save" )
    @ResponseBody
    public void save(@RequestBody List<TestConfigBean> info){
        testConfigService.insertConfig(info);
    }

    @RequestMapping("/update")
    @ResponseBody
    public void update(@RequestBody List<TestConfigBean> info){
        testConfigService.updateConfig(info);
    }
}
