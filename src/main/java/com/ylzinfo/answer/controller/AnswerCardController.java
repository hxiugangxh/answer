package com.ylzinfo.answer.controller;

import com.github.pagehelper.PageInfo;
import com.ylzinfo.answer.bean.AnswerCardBean;
import com.ylzinfo.answer.dto.TableDTO;
import com.ylzinfo.answer.service.AnswerCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("answerCard")
public class AnswerCardController {
    @Autowired
    private AnswerCardService answerCardService;

    @GetMapping("/list")
    public String list(String answerType, Model model, Principal principal) {
        model.addAttribute("answerType", answerType);
        return "question/paper_study";
    }

    @GetMapping("/getByAnswerType")
    @ResponseBody
    public TableDTO<List<AnswerCardBean>> getByAnswerType(AnswerCardBean answerCardBean, int page, int limit) {
        PageInfo<AnswerCardBean> pageInfo = answerCardService.getByTypeAndTitle(answerCardBean, page, limit);
        return new TableDTO<List<AnswerCardBean>>().dataSuccess(pageInfo.getList(), (int) pageInfo.getTotal());
    }
}
