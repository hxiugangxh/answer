package com.ylzinfo.answer.controller;

import com.ylzinfo.answer.service.AnswerErrorService;
import com.ylzinfo.answer.vo.PaperAnalyseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("answerError")
public class AnswerErrorController {
    @Autowired
    private AnswerErrorService answerErrorService;

    @GetMapping("analyse")
    @ResponseBody
    public List<PaperAnalyseVo> analyse(long logId) {
        return answerErrorService.paperAnalyse(logId);
    }
}
