package com.ylzinfo.answer.controller;

import com.ylzinfo.answer.bean.UserBean;
import com.ylzinfo.answer.dao.jpa.UserBeanJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserBeanJpa userBeanJpa;

    @RequestMapping("/me")
    @ResponseBody
    public UserBean me(Principal principal) {
        UserBean userBean = new UserBean();
        userBean.setId(-1);
        if (principal != null) {
            userBean.setDisplayName(principal.getName());
        }

        return userBean;
    }

}
