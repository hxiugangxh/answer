package com.ylzinfo.answer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/refresh")
public class RefreshController {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Integer init = 0;

    @RequestMapping("/init")
    @ResponseBody
    public String refresh() {

        List<String> tableList = Arrays.asList(
                "cm_answer_options",
                "cm_answer_question",
                "cm_answer_card",
                "cm_answer_error",
                "cm_answer_log",
                "cm_dic_code",
                "cm_user"
        );

        if (init == 0) {
            init = 1;
            for (String table : tableList) {
                String dropSQL = "drop table " + table;

                log.info(dropSQL);

                this.jdbcTemplate.update(dropSQL);
            }
        }

        return "初始化成功，请重新启动项目";
    }

}
