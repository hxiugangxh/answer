package com.ylzinfo.answer.controller;

import com.ylzinfo.answer.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    private SignInService signInService;

    @RequestMapping(value = {"", "/index"})
    public String index(Map<String, Object> map, Principal principal) {
        map.put("userName", principal.getName());
        map.put("isSignIn",signInService.getSignInByUserName(principal.getName()));
        return "index";
    }

    @RequestMapping("/login")
    public String login() {

        return "login";
    }

//    @RequestMapping("/loginOut")
//    public String singOut(
//            Map<String, Object> map,
//            @RequestParam(value = "callBackUrl", defaultValue = "") String callBackUrl) {
//
//        map.put("callBackUrl", callBackUrl);
//
//        return "loginout";
//    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpSession session) throws Exception{
        session.invalidate();
        return "redirect:login";
    }

    @RequestMapping("/signIn")
    @ResponseBody
    public void signIn(Principal principal){
        signInService.insertSignInByUserId(principal.getName());
    }

//    @RequestMapping("/isSignIn")
//    @ResponseBody
//    public Map<String,Object> isSignIn(Principal principal){
//        String userName = principal.getName();
//        Map<String,Object> map = new HashMap<>();
//        map.put("isSignIn",signInService.getSignInByUserName(userName));
//        return map;
//    }

}
