package com.ylzinfo.answer.service;

import com.ylzinfo.answer.vo.PaperAnalyseVo;

import java.util.List;

public interface AnswerErrorService {
    List<PaperAnalyseVo> paperAnalyse(long logId);
}
