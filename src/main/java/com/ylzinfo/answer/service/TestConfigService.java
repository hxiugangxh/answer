package com.ylzinfo.answer.service;

import com.ylzinfo.answer.bean.TestConfigBean;

import java.util.List;

public interface TestConfigService {
    List<TestConfigBean> listByCardId(long cardId);

    List<TestConfigBean> getConfigList(long cardId);

    void insertConfig(List<TestConfigBean> list);

    void updateConfig(List<TestConfigBean> list);
}
