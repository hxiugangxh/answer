package com.ylzinfo.answer.service;

import com.github.pagehelper.PageInfo;
import com.ylzinfo.answer.bean.DicCodeBean;
import org.springframework.data.domain.Page;

import java.util.List;

public interface DicService {
    List<DicCodeBean> findAllByCode(String code);

    Page<DicCodeBean> findByCode(String code, int page, int limit);

    PageInfo<DicCodeBean> getByAnswerType(int page, int limit, String answerType);

    PageInfo<DicCodeBean> getByFields(DicCodeBean dicCodeBean, int page, int limit);

    boolean insert(DicCodeBean dicCodeBean);

    boolean delete(List<DicCodeBean> list);

    DicCodeBean findById(long id);

    boolean update(DicCodeBean dicCodeBean);

    List<DicCodeBean> findDicCode();
}
