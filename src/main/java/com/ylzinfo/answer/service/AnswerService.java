package com.ylzinfo.answer.service;

import com.ylzinfo.answer.bean.AnswerCardBean;
import com.ylzinfo.answer.bean.AnswerLogBean;
import com.ylzinfo.answer.bean.AnswerQuestionBean;
import com.ylzinfo.answer.entity.AnswerCard;
import com.ylzinfo.answer.vo.ErrorSearchVo;
import com.ylzinfo.answer.vo.SearchVo;

import java.util.List;
import java.util.Map;

public interface AnswerService {
    Map<String, Object> listAnswerCardByParam(Integer pn, Integer pageSize, SearchVo searchVo);

    //    void saveAnswerLog(String userId, Long answerCardId, String errorQuestionIdArr);
    void saveAnswerLog(AnswerLogBean answerLogBean);

    AnswerCardBean errorStudy(List<String> answerLogIdList);

    void batchDelAnswerCard(String selectId);

    Map<String, Object> listAnswerLogByParam(Integer pn, Integer pageSize, SearchVo searchVo);

    void batchDelAnswerLog(String selectId);

    AnswerCard test(long cardId);

    List<AnswerQuestionBean> resetNum(List<AnswerQuestionBean> list, boolean query);

    Map<String,Object> findErrorList(Integer pn, Integer pageSize, ErrorSearchVo errorSearchVo);

}
