package com.ylzinfo.answer.service;

import com.github.pagehelper.PageInfo;
import com.ylzinfo.answer.bean.AnswerCardBean;

public interface AnswerCardService {
    PageInfo<AnswerCardBean> getByTypeAndTitle(AnswerCardBean answerCardBean, int page, int limit);

    PageInfo<AnswerCardBean> getAll(int page, int limit);

    AnswerCardBean getOne(long id);
}
