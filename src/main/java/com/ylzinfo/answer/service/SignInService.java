package com.ylzinfo.answer.service;

public interface SignInService {

    void insertSignInByUserId(String userName);

    boolean getSignInByUserName(String userName);
}
