package com.ylzinfo.answer.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ylzinfo.answer.bean.*;
import com.ylzinfo.answer.dao.jpa.AnswerCardBeanJpa;
import com.ylzinfo.answer.dao.jpa.AnswerErrorBeanJpa;
import com.ylzinfo.answer.dao.jpa.AnswerLogBeanJpa;
import com.ylzinfo.answer.dao.mapper.AnswerCardMapper;
import com.ylzinfo.answer.dao.mapper.AnswerMapper;
import com.ylzinfo.answer.dao.mapper.AnswerOptionsMapper;
import com.ylzinfo.answer.entity.AnswerCard;
import com.ylzinfo.answer.service.AnswerService;
import com.ylzinfo.answer.service.TestConfigService;
import com.ylzinfo.answer.vo.ErrorSearchVo;
import com.ylzinfo.answer.vo.SearchVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Transactional
@Service("answerService")
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerMapper answerMapper;
    @Autowired
    private AnswerOptionsMapper answerOptionsMapper;
    @Autowired
    private AnswerCardMapper answerCardMapper;
    @Autowired
    private AnswerCardBeanJpa answerCardBeanJpa;
    @Autowired
    private AnswerLogBeanJpa answerLogBeanJpa;
    @Autowired
    private AnswerErrorBeanJpa answerErrorBeanJpa;
    @Autowired
    private TestConfigService testConfigService;

    @Override
    public Map<String, Object> listAnswerLogByParam(Integer pn, Integer pageSize, SearchVo searchVo) {
        Map<String, Object> dataMap = new HashMap<>();

        PageHelper.startPage(pn, pageSize);
        List<AnswerLogBean> list = answerMapper.listAnswerLogByParam(searchVo);
        PageInfo<AnswerLogBean> pageInfo = new PageInfo<>(list);

        dataMap.put("data", list);
        dataMap.put("count", pageInfo.getTotal());
        dataMap.put("code", 0);

        return dataMap;
    }

    @Override
    public void batchDelAnswerLog(String selectId) {
        for (String id : selectId.split(",")) {
            AnswerLogBean answerLogBean = answerLogBeanJpa.findOne(new Long(id));

            answerErrorBeanJpa.deleteAllByAnswerLogId(answerLogBean.getId());
            answerLogBeanJpa.delete(answerLogBean);
        }
    }

    @Override
    public AnswerCard test(long cardId) {
        AnswerCard answerCard = answerCardMapper.findById(cardId);
        List<TestConfigBean> testConfigBeans = testConfigService.listByCardId(cardId);
        if (CollUtil.isEmpty(testConfigBeans)) {
            testConfigBeans.add(new TestConfigBean(null, null, "SINGLE", 5));
            testConfigBeans.add(new TestConfigBean(null, null, "FILL_BLANK", 5));
            testConfigBeans.add(new TestConfigBean(null, null, "MULTI", 5));
            testConfigBeans.add(new TestConfigBean(null, null, "ANSWER", 5));
            testConfigBeans.add(new TestConfigBean(null, null, "JUDGE", 5));
        }
        List<AnswerQuestionBean> answerQuestionBeans = answerMapper.listRandom(cardId, testConfigBeans);
        List<AnswerOptionsBean> optionList = answerOptionsMapper.findByQuestionIdIn(answerQuestionBeans);
        for (AnswerQuestionBean answerQuestionBean : answerQuestionBeans) {
            for (Iterator<AnswerOptionsBean> iterator = optionList.iterator(); iterator.hasNext(); ) {
                AnswerOptionsBean next = iterator.next();
                if (next.getAnswerQuestionId().equals(answerQuestionBean.getId())) {
                    answerQuestionBean.getAnswerOptionsBeanList().add(next);
                    iterator.remove();
                }
            }
        }
        answerCard.setAnswerQuestionBeanList(resetNum(answerQuestionBeans, true));
        return answerCard;
    }

    @Override
    public List<AnswerQuestionBean> resetNum(List<AnswerQuestionBean> list, boolean query) {
        Integer single = 0;
        Integer multi = 0;
        Integer judge = 0;
        Integer fillBlank = 0;
        Integer answer = 0;
        for (AnswerQuestionBean answerQuestionBean : list) {
            if (!query) {
                answerQuestionBean.setId(null);
            }
            // 重置题号
            if (answerQuestionBean.getAnswerType().equals("SINGLE")) {
                answerQuestionBean.setNum(++single);
            } else if (answerQuestionBean.getAnswerType().equals("MULTI")) {
                answerQuestionBean.setNum(++multi);
            } else if (answerQuestionBean.getAnswerType().equals("JUDGE")) {
                answerQuestionBean.setNum(++judge);
            } else if (answerQuestionBean.getAnswerType().equals("FILL_BLANK")) {
                answerQuestionBean.setNum(++fillBlank);
            } else if (answerQuestionBean.getAnswerType().equals("ANSWER")) {
                answerQuestionBean.setNum(++answer);
            }
        }
        return list;
    }

    @Override
    public Map<String, Object> listAnswerCardByParam(Integer pn, Integer pageSize, SearchVo searchVo) {
        Map<String, Object> dataMap = new HashMap<>();

        PageHelper.startPage(pn, pageSize);
        List<AnswerCardBean> list = answerMapper.listAnswerCardByParam(searchVo);
        PageInfo<AnswerCardBean> pageInfo = new PageInfo<>(list);

        dataMap.put("data", list);
        dataMap.put("count", pageInfo.getTotal());
        dataMap.put("code", 0);

        return dataMap;
    }

//    @Override
//    public void saveAnswerLog(String userId, Long answerCardId, String errorQuestionIdArr) {
//        Integer errorNum = (StringUtils.isNotEmpty(errorQuestionIdArr)) ? errorQuestionIdArr.split(",").length : 0;
//        AnswerLogBean answerLogBean = new AnswerLogBean(null, userId, answerCardId, errorNum, new Date());
//        answerLogBeanJpa.save(answerLogBean);
//
//        if (StringUtils.isNotEmpty(errorQuestionIdArr)) {
//            List<AnswerErrorBean> answerErrorBeanList = new ArrayList<>();
//            for (String answerQuestionId : errorQuestionIdArr.split(",")) {
//                answerErrorBeanList.add(new AnswerErrorBean(null, answerLogBean.getId(), answerCardId, answerQuestionId));
//            }
//
//            answerErrorBeanJpa.save(answerErrorBeanList);
//        }
//    }

    @Override
    public void saveAnswerLog(AnswerLogBean answerLogBean) {
        answerLogBean.setCreateTime(new Date());
        answerLogBeanJpa.save(answerLogBean);
        for (AnswerErrorBean answerErrorBean : answerLogBean.getErrorList()) {
            answerErrorBean.setAnswerLogId(answerLogBean.getId());
            answerErrorBean.setAnswerCardId(answerLogBean.getAnswerCardId());
        }
        answerErrorBeanJpa.save(answerLogBean.getErrorList());
    }

    @Override
    public AnswerCardBean errorStudy(List<String> answerLogIdList) {
        AnswerCardBean errorAnswerCardBean = new AnswerCardBean();
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssS");
        errorAnswerCardBean.setTitle("错题集编号：" + df.format(new Date()));
        errorAnswerCardBean.setAnswerType("ERROR");
        errorAnswerCardBean.setAnswerTime(1000);
        errorAnswerCardBean.setCreateTime(new Date());

        List<AnswerQuestionBean> tmpList = new ArrayList<>();
        for (String answerLogId : answerLogIdList) {
            AnswerLogBean answerLogBean = answerMapper.findAnswerLogBeanById(answerLogId);

            List<Long> answerQuestionIdList = answerMapper.listAnswerQuestionId(answerLogBean.getId());

            List<AnswerQuestionBean> answerQuestionBeanList = answerMapper
                    .listErrorAnswerQuestionByIdIn(answerQuestionIdList);
            for (AnswerQuestionBean answerQuestionBean : answerQuestionBeanList) {
                List<AnswerOptionsBean> answerOptionsBeanList = answerMapper
                        .listAnswerOptionsByAnswerQuestionId(answerQuestionBean.getId());

                answerQuestionBean.setAnswerOptionsBeanList(answerOptionsBeanList);
            }
            tmpList.addAll(answerQuestionBeanList);
        }
        errorAnswerCardBean.setAnswerQuestionBeanList(resetNum(tmpList, false));

        answerCardBeanJpa.save(errorAnswerCardBean);
        return errorAnswerCardBean;
    }

    @Override
    public void batchDelAnswerCard(String selectId) {
        for (String id : selectId.split(",")) {
            answerCardBeanJpa.delete(new Long(id));
        }
    }

    @Override
    public Map<String,Object> findErrorList(Integer pn, Integer pageSize, ErrorSearchVo errorSearchVo){
        Map<String, Object> dataMap = new HashMap<>();

        PageHelper.startPage(pn, pageSize);
        List<AnswerCardBean> list = answerMapper.findErrorList(errorSearchVo);
        PageInfo<AnswerCardBean> pageInfo = new PageInfo<>(list);

        dataMap.put("data", list);
        dataMap.put("count", pageInfo.getTotal());
        dataMap.put("code", 0);

        return dataMap;
    }

}
