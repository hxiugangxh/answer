package com.ylzinfo.answer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ylzinfo.answer.bean.DicCodeBean;
import com.ylzinfo.answer.dao.jpa.DicCodeBeanJpa;
import com.ylzinfo.answer.dao.mapper.DicCodeMapper;
import com.ylzinfo.answer.service.DicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service("dicService")
public class DicServiceImpl implements DicService {

    @Autowired
    private DicCodeBeanJpa dicCodeBeanJpa;
    @Autowired
    private DicCodeMapper dicCodeMapper;

    @Override
    public List<DicCodeBean> findAllByCode(String code) {
        return dicCodeBeanJpa.findAllByCode(code);
    }

    @Override
    public Page<DicCodeBean> findByCode(String code, int page, int limit) {
        return dicCodeBeanJpa.findByCodeOrderByCode(code, new PageRequest(page, limit));
    }

    @Override
    public PageInfo<DicCodeBean> getByAnswerType(int page, int limit, String answerType) {
        PageHelper.startPage(page, limit);
        return new PageInfo<>(dicCodeMapper.getByAnswerType(answerType));
    }

    @Override
    public PageInfo<DicCodeBean> getByFields(DicCodeBean dicCodeBean, int page, int limit) {
        PageHelper.startPage(page, limit);
        return new PageInfo<>(dicCodeMapper.getByFields(dicCodeBean));
    }

    @Override
    public boolean insert(DicCodeBean dicCodeBean) {
        return dicCodeBeanJpa.save(dicCodeBean) != null;
    }

    @Override
    public boolean delete(List<DicCodeBean> list) {
        dicCodeBeanJpa.delete(list);
        return true;
    }

    @Override
    public DicCodeBean findById(long id) {
        return dicCodeBeanJpa.findOne(id);
    }

    @Override
    public boolean update(DicCodeBean dicCodeBean) {
        return dicCodeBeanJpa.save(dicCodeBean) != null;
    }

    @Override
    public List<DicCodeBean> findDicCode(){
        return dicCodeMapper.findDicCode();
    }

}
