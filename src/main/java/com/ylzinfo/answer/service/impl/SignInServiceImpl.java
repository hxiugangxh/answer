package com.ylzinfo.answer.service.impl;

import cn.hutool.core.date.DateUtil;
import com.ylzinfo.answer.bean.SignInBean;
import com.ylzinfo.answer.dao.mapper.SignInMapper;
import com.ylzinfo.answer.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("signInService")
public class SignInServiceImpl implements SignInService {

    @Autowired
    private SignInMapper signInMapper;

    @Override
    public void insertSignInByUserId(String userName){
        SignInBean signInBean = new SignInBean();
        signInBean.setUserName(userName);
        signInBean.setCreateTime(new Date());
        signInMapper.insertSignInByUserId(signInBean);
    }

    @Override
    public boolean getSignInByUserName(String userName){

        Date date = new Date();
        //DateUtil.beginOfDay(date) 一天的开始
        //DateUtil.endOfDay(date) 一天的结束
        if (signInMapper.getSignInByUserName(userName,DateUtil.beginOfDay(date),DateUtil.endOfDay(date))!=null){
            return true;
        }
        return false;
    }
}
