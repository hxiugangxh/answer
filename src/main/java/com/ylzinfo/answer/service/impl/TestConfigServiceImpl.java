package com.ylzinfo.answer.service.impl;

import com.ylzinfo.answer.bean.TestConfigBean;
import com.ylzinfo.answer.dao.jpa.TestConfigRepo;
import com.ylzinfo.answer.dao.mapper.AnswerConfigMapper;
import com.ylzinfo.answer.service.TestConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestConfigServiceImpl implements TestConfigService {
    @Autowired
    private TestConfigRepo testConfigRepo;
    @Autowired
    private AnswerConfigMapper answerConfigMapper;

    @Override
    public List<TestConfigBean> listByCardId(long cardId) {
        return testConfigRepo.findByCardId(cardId);
    }

    @Override
    public List<TestConfigBean> getConfigList(long cardId) {
        return answerConfigMapper.getConfigList(cardId);
    }

    @Override
    public void insertConfig(List<TestConfigBean> list) {
        answerConfigMapper.insertConfig(list);
    }

    @Override
    public void updateConfig(List<TestConfigBean> list){
        answerConfigMapper.updateConfig(list);
    }
}
