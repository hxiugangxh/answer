package com.ylzinfo.answer.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.ylzinfo.answer.dao.mapper.AnswerErrorMapper;
import com.ylzinfo.answer.service.AnswerErrorService;
import com.ylzinfo.answer.vo.PaperAnalyseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerErrorServiceImpl implements AnswerErrorService {
    @Autowired
    private AnswerErrorMapper answerErrorMapper;

    @Override
    public List<PaperAnalyseVo> paperAnalyse(long logId) {
        List<String> questionTypeList = CollUtil.newArrayList("SINGLE", "MULTI", "FILL_BLANK", "JUDGE", "TOTAL");
        return answerErrorMapper.analyseByLogId(logId, questionTypeList);
    }
}
