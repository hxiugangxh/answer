package com.ylzinfo.answer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ylzinfo.answer.bean.AnswerCardBean;
import com.ylzinfo.answer.dao.jpa.AnswerCardBeanJpa;
import com.ylzinfo.answer.dao.mapper.AnswerCardMapper;
import com.ylzinfo.answer.service.AnswerCardService;
import com.ylzinfo.answer.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class AnswerCardServiceImpl implements AnswerCardService {
    @Autowired
    private AnswerCardMapper answerCardMapper;
    @Autowired
    private AnswerCardBeanJpa answerCardBeanJpa;
    @Autowired
    private AnswerService answerService;

    @Override
    public PageInfo<AnswerCardBean> getByTypeAndTitle(AnswerCardBean answerCardBean, int page, int limit) {
        PageHelper.startPage(page, limit);
        return new PageInfo<>(answerCardMapper.getByAnswerTypeAndTitle(answerCardBean.getAnswerType(), answerCardBean.getTitle()));
    }

    @Override
    public PageInfo<AnswerCardBean> getAll(int page, int limit) {
        PageHelper.startPage(page, limit);
        return new PageInfo<>(answerCardMapper.getAll());
    }

    @Override
    public AnswerCardBean getOne(long id) {
        AnswerCardBean answerCardBean = answerCardBeanJpa.findOne(id);
        answerService.resetNum(answerCardBean.getAnswerQuestionBeanList(), true);
        return answerCardBean;
    }
}
