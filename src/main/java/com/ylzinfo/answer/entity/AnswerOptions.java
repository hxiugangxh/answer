package com.ylzinfo.answer.entity;

import lombok.Data;

@Data
public class AnswerOptions {
    private Long id;

    private Long answerQuestionId;

    private String selectOption;

    private String optionInfo;
}
