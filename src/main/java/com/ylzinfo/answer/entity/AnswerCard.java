package com.ylzinfo.answer.entity;

import com.ylzinfo.answer.bean.AnswerQuestionBean;
import lombok.Data;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class AnswerCard {
    private Long id;

    private Long userId;

    private String title;

    private String answerType;

    private Integer answerTime;

    private Date createTime;

    @Transient
    private String answerTypeName;
    @Transient
    private List<AnswerQuestionBean> answerQuestionBeanList = new ArrayList<>();
}
