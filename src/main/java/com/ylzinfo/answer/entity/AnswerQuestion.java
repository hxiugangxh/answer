package com.ylzinfo.answer.entity;

import com.ylzinfo.answer.bean.AnswerOptionsBean;
import lombok.Data;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Data
public class AnswerQuestion {
    private Long id;

    private Long answerCardId;

    private Integer num;

    private String question;

    private String answer;

    private String answerType;

    private String parseInfo;

    @Transient
    private List<AnswerOptionsBean> answerOptionsBeanList = new ArrayList<>();

}
