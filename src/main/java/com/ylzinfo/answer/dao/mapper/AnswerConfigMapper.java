package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.bean.TestConfigBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnswerConfigMapper {
    List<TestConfigBean> getConfigList(Long id);

    void insertConfig(@Param("list") List<TestConfigBean> list);

    void updateConfig(@Param("list") List<TestConfigBean> list);
}
