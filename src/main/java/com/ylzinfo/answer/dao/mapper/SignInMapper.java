package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.bean.SignInBean;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface SignInMapper {

    void insertSignInByUserId(SignInBean signInBean);
    SignInBean getSignInByUserName(@Param("userName") String userName,@Param("beginOfDay") Date beginOfDay, @Param("endOfDay") Date endOfDay);
}
