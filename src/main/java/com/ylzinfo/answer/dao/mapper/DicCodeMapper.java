package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.bean.DicCodeBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DicCodeMapper {
    List<DicCodeBean> getByAnswerType(@Param("answerType") String answerType);

    List<DicCodeBean> getByFields(@Param("dicCode")DicCodeBean dicCodeBean);

    List<DicCodeBean> findDicCode();
}
