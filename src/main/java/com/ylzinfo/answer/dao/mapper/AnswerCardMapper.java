package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.bean.AnswerCardBean;
import com.ylzinfo.answer.entity.AnswerCard;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnswerCardMapper {
    List<AnswerCardBean> getByAnswerTypeAndTitle(@Param("answerType") String answerType, @Param("title") String title);

    List<AnswerCardBean> getAll();

    AnswerCard findById(@Param("id") long id);
}
