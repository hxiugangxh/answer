package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.vo.PaperAnalyseVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnswerErrorMapper {
    List<PaperAnalyseVo> analyseByLogId(@Param("logId") long logId, @Param("questionType") List<String> questionType);
}
