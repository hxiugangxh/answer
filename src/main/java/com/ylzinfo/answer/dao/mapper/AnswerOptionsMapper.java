package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.bean.AnswerOptionsBean;
import com.ylzinfo.answer.bean.AnswerQuestionBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AnswerOptionsMapper {
    List<AnswerOptionsBean> findByQuestionIdIn(@Param("questionList") List<AnswerQuestionBean> list);
}
