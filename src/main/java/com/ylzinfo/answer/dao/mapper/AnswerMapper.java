package com.ylzinfo.answer.dao.mapper;

import com.ylzinfo.answer.bean.*;
import com.ylzinfo.answer.vo.ErrorSearchVo;
import com.ylzinfo.answer.vo.SearchVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnswerMapper {
    List<AnswerCardBean> listAnswerCardByParam(SearchVo searchVo);

    List<Long> listAnswerQuestionId(@Param("answerLogId") Long answerLogId);

    List<AnswerLogBean> listAnswerLogByParam(SearchVo searchVo);

    AnswerLogBean findAnswerLogBeanById(@Param("answerLogId") String answerLogId);

    List<AnswerQuestionBean> listErrorAnswerQuestionByIdIn(@Param("answerQuestionIdList") List<Long> answerQuestionIdList);

    List<AnswerOptionsBean> listAnswerOptionsByAnswerQuestionId(@Param("answerQuestionId") Long answerQuestionId);

    List<AnswerQuestionBean> listRandom(@Param("cardId") long cardId,
                                        @Param("testConfig") List<TestConfigBean> testConfig);
    List<AnswerCardBean> findErrorList(ErrorSearchVo errorSearchVo);

}