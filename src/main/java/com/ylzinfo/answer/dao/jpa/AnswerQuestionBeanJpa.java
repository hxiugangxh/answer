package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.AnswerQuestionBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AnswerQuestionBeanJpa extends CrudRepository<AnswerQuestionBean, Long> {

    @Query(value = "select * from cm_answer_question where id in (:questionCardIdList)", nativeQuery = true)
    List<AnswerQuestionBean> listErrorAnswerQuestion(@Param("questionCardIdList") List<Long> questionCardIdList);
}
