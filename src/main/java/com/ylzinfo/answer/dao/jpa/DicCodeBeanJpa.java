package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.DicCodeBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DicCodeBeanJpa extends CrudRepository<DicCodeBean, Long> {

    List<DicCodeBean> findAllByCode(String code);

    Page<DicCodeBean> findByCodeOrderByCode(String code, Pageable pageable);
}
