package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.AnswerErrorBean;
import org.springframework.data.repository.CrudRepository;

public interface AnswerErrorBeanJpa extends CrudRepository<AnswerErrorBean, Long> {
    void deleteAllByAnswerLogId(Long answerLogId);
}
