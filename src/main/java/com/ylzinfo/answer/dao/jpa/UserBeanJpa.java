package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.UserBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserBeanJpa extends CrudRepository<UserBean, Long> {

    UserBean findByUsername(String userName);

}
