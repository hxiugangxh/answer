package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.AnswerLogBean;
import org.springframework.data.repository.CrudRepository;

public interface AnswerLogBeanJpa extends CrudRepository<AnswerLogBean, Long> {
}
