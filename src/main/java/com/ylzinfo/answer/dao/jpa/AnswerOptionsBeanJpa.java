package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.AnswerOptionsBean;
import org.springframework.data.repository.CrudRepository;

public interface AnswerOptionsBeanJpa extends CrudRepository<AnswerOptionsBean, Long> {
}
