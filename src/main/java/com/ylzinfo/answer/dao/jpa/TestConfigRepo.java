package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.TestConfigBean;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TestConfigRepo extends JpaRepository<TestConfigBean, Long> {
    List<TestConfigBean> findByCardId(long cardId);
}
