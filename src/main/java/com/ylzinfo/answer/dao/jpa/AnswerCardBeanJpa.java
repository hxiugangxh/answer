package com.ylzinfo.answer.dao.jpa;

import com.ylzinfo.answer.bean.AnswerCardBean;
import org.springframework.data.repository.CrudRepository;

public interface AnswerCardBeanJpa extends CrudRepository<AnswerCardBean, Long> {
}
